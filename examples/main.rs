extern crate ecs_disk_manager;
extern crate log;

use ecs_disk_manager::prelude::*;
use log::{Record, Level, Metadata};
use std::env::args;
use std::io;
use std::process;

struct SimpleLogger;

impl log::Log for SimpleLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= Level::Trace
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            println!("{} - {}", record.level(), record.args());
        }
    }

    fn flush(&self) {}
}

use log::{SetLoggerError, LevelFilter};

static LOGGER: SimpleLogger = SimpleLogger;

pub fn init() -> Result<(), SetLoggerError> {
    log::set_logger(&LOGGER)
        .map(|()| log::set_max_level(LevelFilter::Trace))?;

    Ok(())
}

fn main() {
    init().unwrap();

    let mut devices = match DeviceWorld::new() {
        Ok(devices) => devices,
        Err(why) => {
            eprintln!("failed to get devices: {:?}", why);
            process::exit(1);
        }
    };

    read_devices(&devices);

    let disk_arg = args().nth(1).expect("expected a disk");

    // Take an existing disk, wipe the partition table with a new label, and add new partitions to it.
    devices.find_disk(&disk_arg, |disk| {
        // This will perform wipefs and create a new partition table on this disk.
        disk.mklabel(PartitionTable::Gpt);

        // Add the EFI system partition.
        let mut start = Sector::Start;
        let mut end = Sector::Megabyte(512);
        disk.partition_add(PartitionBuilder::new(start, end)
            .variant(FileSystemBuilder::new(FileSystem::Fat32)
                .label("Boot".into()))
        );

        // Then create a LVM on LUKS-encrypted drive that contains an inner
        // root and swap logical partition, which we can install Linux onto.
        start = end;
        end = Sector::End;

        let root = PartitionBuilder::new(Sector::Start, Sector::MegabyteFromEnd(4096))
            .variant(FileSystemBuilder::new(FileSystem::Ext4));

        let swap = PartitionBuilder::new(Sector::MegabyteFromEnd(4096), Sector::End)
            .variant(FileSystemBuilder::new(FileSystem::Swap));

        let lvm_data = LvmBuilder::new("lvmdata".into())
            .partition(root, "root".into())
            .partition(swap, "swap".into());

        let luks_data = LuksBuilder::new("luksdata".into())
            .password("pass".into())
            .file_system(lvm_data);

        disk.partition_add(PartitionBuilder::new(start, end).variant(luks_data));
    });

    // Apply the specified changes, and check for errors.
    check_errors(devices.dispatch());

    // Print the new information.
    devices.find_disk(&disk_arg, |disk| {
        read_device(disk.get_data());
    });
}

fn check_errors(errors: Option<Vec<io::Error>>) {
    if let Some(errors) = errors {
        for error in errors {
            eprintln!("disk error: {}", error);
        }
        process::exit(1);
    }
}

fn read_devices(world: &DeviceWorld) {
    // Retrieve information about all the disks in the world.
    for device in world.get_disks() {
        read_device(device.get_data());
    }
}

fn read_device(data: DiskComponents) {
    println!("Device {:?}", data.get_device_path());
    println!("  Model:   {:?}", data.get_model_string());
    println!("  Table:   {:?}", data.get_partition_table());
    // println!("  Type:    {:?}", data.get_type());
    println!("  Sectors: {:?}", data.get_sectors());

    match data.get_partitions() {
        Some(partitions) => {
            println!("Partitions found:");
            for partition in partitions {
                let mut data = partition.get_data();
                println!("    {:?}:", data.get_device_path());
                println!("      Type:       {:?}", data.get_type());
                println!("      Filesystem: {:?}", data.get_file_system());
                println!("      Sectors:    {}", data.get_sectors());
                println!("        Start:    {}", data.get_sector_start());
                println!("        End:      {}", data.get_sector_end());
            }
        }
        None => {
            println!("  Partitions found: None");
        }
    };
}
