# ECS Disk Manager

This is an experiment to create a disk management library in Rust using an ECS architecture. A modern disk management library is highly complex to implement. Rust and the ECS architecture can make this significantly easier to achieve through the type system, algebraic data types, Cargo, and crates like specs.

## Reasoning

Distinst was the inspiration for this project. Time constraints for the 18.04 release meant working to produce a product that was good enough for most needs in an installer. The disk management capabilities of distinst were heavily coupled with the project, and it could not handle exotic configurations such as FS on LUKS, or FS on LUKS on LVM on LUKS. Luckily, much of the disk-related code written for distinst is salvageable and will be reused here.

This will ultimately be used to replace the disk management capabilities of distinst, and will be flexible enough for the creation of new disk management applications written in Rust. Alternatives such as GParted do not support Wayland or handle LVM / LUKS functionality very well. We could do better if we have a foundation (this) to build upon.

## Features

- All internal state is managed via an entity-component system.
    - Enables cache coherency, due to arena allocations.
    - Fast look-ups of entities and their components.
    - Flexible handling of data, enhancing capabilities with minimal code.
- Queries disk information from the UDisks2 DBus API, falling back to libparted.
    - UDisks2 is very fast in comparison to libparted.
- Changes to partition tables are performed with libparted
    - Operations are preformed serially.
    - LUKS PVs, LVM VGs, and LVM LVs are created serially.
- Partitions are formatted in parallel.
    - Once all partitions have been removed / modified / crated, they are formatted in parallel.
    - It doesn't matter whether they're logical or physical.
- The creation of recursive partition hierarchies
    - LVM on LVM / LUKS
    - FS on LVM / LUKS
    - LUKS on LVM

## TODO

- Resizing and moving partitions.
- Separate crates from the distinst project, so that they can be used here.
- API for handling automatic mount / dismounts.
- Deactivate LVM volume groups before committing.
- Deactivate LUKS devices before committing.
- Activate all defined LVM and LUKS constructs.
- Detect existing LVM and LUKS device layouts.
- Support manipulating existing LVM and LUKS constructs.
- Support removing LVM and LUKS constructs.
- Complete the API for accessing details on partitions and devices.
- Support for finding a specific partition.
- Support assigning the ESP partition flag.

## Examples

See the examples directory. Currently includes an example which configures a theoretical EFI Linux installation with the root and swap partition on a LVM on LUKS setup.
