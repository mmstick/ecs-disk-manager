//! A collection of external commands used throughout the program.

// TODO: Migrate this to external crate.

pub use external_::*;
use proc_mounts::{MountList, SwapList};
use std::io;
use std::path::Path;
use sys_mount::*;

fn remove_encrypted_device(device: &Path) -> io::Result<()> {
    let mounts = MountList::new().expect("failed to get mounts in deactivate_device_maps");
    let swaps = SwapList::new().expect("failed to get swaps in deactivate_device_maps");
    let umount = move |vg: &str| -> io::Result<()> {
        for lv in lvs(vg)? {
            if let Some(mount) = mounts.get_mount_by_source(&lv) {
                info!("unmounting logical volume mounted at {}", mount.dest.display());
                unmount(&mount.dest, UnmountFlags::empty())?;
            } else if let Ok(lv) = lv.canonicalize() {
                if swaps.get_swapped(&lv) {
                    swapoff(&lv)?;
                }
            }
        }

        Ok(())
    };

    let volume_map = pvs()?;
    let to_deactivate = physical_volumes_to_deactivate(&[device]);

    for pv in &to_deactivate {
        let dev = CloseBy::Path(&pv);
        match volume_map.get(pv) {
            Some(&Some(ref vg)) => umount(vg).and_then(|_| {
                info!("removing pre-existing LUKS + LVM volumes on {:?}", device);
                vgdeactivate(vg)
                    .and_then(|_| vgremove(vg))
                    .and_then(|_| pvremove(pv))
                    .and_then(|_| cryptsetup_close(dev))
            })?,
            Some(&None) => {
                cryptsetup_close(dev)?
            }
            None => (),
        }
    }

    if let Some(ref vg) = volume_map.get(device).and_then(|x| x.as_ref()) {
        info!("removing pre-existing LVM volumes on {:?}", device);
        umount(vg)
            .and_then(|_| vgremove(vg))?
    }

    Ok(())
}

/// Creates a LUKS partition from a physical partition. This could be either a LUKS on LVM
/// configuration, or a LVM on LUKS configurations.
pub fn cryptsetup_encrypt(device: &Path, pass: &str) -> io::Result<()> {
    remove_encrypted_device(device)?;

    info!(
        "cryptsetup is encrypting {}",
        device.display(),
    );

    exec(
        "cryptsetup",
        Some(&append_newline(pass.as_bytes())),
        None,
        &[
            "-s".into(),
            "512".into(),
            "luksFormat".into(),
            device.into(),
        ],
    )
}

/// Opens an encrypted partition and maps it to the pv name.
pub fn cryptsetup_open(device: &Path, pv: &str, pass: &str) -> io::Result<()> {
    deactivate_devices(&[device])?;
    info!(
        "cryptsetup is opening {} with pv {}",
        device.display(),
        pv,
    );

    exec(
        "cryptsetup",
        Some(&append_newline(pass.as_bytes())),
        None,
        &["open".into(), device.into(), pv.into()],
    )
}

/// Append a newline to the input (used for the password)
fn append_newline(input: &[u8]) -> Vec<u8> {
    let mut input = input.to_owned();
    input.reserve_exact(1);
    input.push(b'\n');
    input
}
