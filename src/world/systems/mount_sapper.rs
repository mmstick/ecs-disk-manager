use specs::prelude::*;
use world::components::*;

pub struct MountSapper;

impl<'a> System<'a> for MountSapper {
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, DeviceComponent>,
        WriteStorage<'a, PartitionComponent>,
        WriteStorage<'a, PartitionName>,
    );

    fn run(&mut self, (entities, paths, partitions, labels): Self::SystemData) {
        // TODO: Implement this.
    }
}
