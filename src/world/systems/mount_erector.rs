use specs::prelude::*;
use world::components::*;

pub struct MountErector;

impl<'a> System<'a> for MountErector {
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, DeviceComponent>,
        WriteStorage<'a, PartitionComponent>,
        WriteStorage<'a, PartitionName>,
    );

    fn run(&mut self, (entities, paths, partitions, labels): Self::SystemData) {
        // TODO: Implement this.
    }
}
