use disk_ops::mklabel;
use specs::prelude::*;
use specs::Write as ResWrite;
use std::time::Instant;
use super::super::*;

/// Applies `mklabel` operations to disks in the world.
pub struct TableMaker;

impl<'a> System<'a> for TableMaker {
    type SystemData = (
        WriteStorage<'a, FormatTable>,
        ReadStorage<'a, DeviceComponent>,
        WriteStorage<'a, DiskComponent>,
        WriteStorage<'a, Partitions>,
        ResWrite<'a, DiskErrors>,
        Entities<'a>,
    );

    fn run(&mut self, (mut tables, devices, mut disks, mut partitions, mut errors, entities): Self::SystemData) {
        info!("TableMaker: starting execution");
        let start = Instant::now();

        for (device, disk, partitions, table) in (&devices, &mut disks, &mut partitions, tables.drain()).join() {
            match mklabel(&device.device, table.0) {
                Ok(()) => {
                    // Mark the partition entities to be deleted.
                    partitions.0.drain(..).for_each(|entity| {
                        entities.delete(entity);
                    });

                    // TODO: Also remove any LUKS or LVM devices on a partition.

                    disk.partition_table = Some(table.0);
                }
                Err(why) => errors.0.push(why),
            }
        }

        info!("TableMaker: job complete in {:?}", Instant::now() - start);
    }
}
