use external::block::mkfs;
use specs::prelude::*;
use std::time::Instant;
use super::super::*;

pub struct Formatter;

impl<'a> System<'a> for Formatter {
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, DeviceComponent>,
        WriteStorage<'a, FormatWith>,
        WriteStorage<'a, FileSystemComponent>,
        Write<'a, DiskErrors>,
    );

    fn run(
        &mut self,
        (entities, devices, mut format, mut file_systems, mut errors): Self::SystemData,
    ) {
        info!("Formatter: starting execution");
        let start = Instant::now();

        for (entity, dev, format) in (&*entities, &devices, format.drain()).join() {
            info!("Formatter: formatting {:?} with {:?}", &dev.device, format.0);
            match mkfs(&dev.device, format.0) {
                Ok(()) => {
                    file_systems.remove(entity);
                    file_systems.insert(entity, FileSystemComponent(format.0));
                },
                Err(why) => {
                    errors.0.push(why);
                }
            }
        }

        info!("Formatter: finished job in {:?}", Instant::now() - start);
    }
}
