use external::*;
use super::super::*;
use disk_types::SectorExt;
use disk_ops::{create_partition, get_partition_id_and_path, move_partition, resize_partition, remove_partition_by_sector,
    PartitionCreate};
use libparted::Device as PedDevice;
use misc::*;
use specs::prelude::*;
use specs::Write as ResWrite;
use std::io;
use std::path::{Path, PathBuf};
use std::time::Instant;

pub struct Partitioner;

impl<'a> System<'a> for Partitioner {
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, DeviceComponent>,
        WriteStorage<'a, PartitionModifications>,
        WriteStorage<'a, Partitions>,
        WriteStorage<'a, PartitionComponent>,
        WriteStorage<'a, FormatWith>,
        WriteStorage<'a, FileSystemComponent>,
        WriteStorage<'a, LuksComponent>,
        WriteStorage<'a, LvmComponent>,
        ResWrite<'a, DiskErrors>,
    );

    fn run(
        &mut self,
        (
            entities,
            mut devices,
            mut modifications,
            mut partitions,
            mut partition_comps,
            mut format_with,
            mut filesystems,
            mut luks,
            mut lvms,
            mut errors,
        ): Self::SystemData,
    ) {
        info!("Partitioner: starting execution");
        let start = Instant::now();

        let mut actions = Vec::new();
        for (entity, device, _partitions, modification) in
            (&*entities, &mut devices, &mut partitions, modifications.drain()).join()
        {
            actions.push((entity, device.device.clone(), modification));
        }

        for (entity, device, mut modification) in actions {
            // The PedDevice enables manipulating the partition table.
            let mut ped;
            let ped_device = match open_device(&device) {
                Ok(device) => {
                    ped = device;
                    &mut ped
                },
                Err(why) => {
                    eprintln!("failed to open device: {}", why);
                    break
                }
            };

            // The first step is to remove partitions from the device.
            {
                let partitions_to_remove = modification.remove.drain(..)
                    .map(|entity| partition_comps.get(entity).map(|p| (entity, p)));

                let mut sectors = Vec::new();
                for partition in partitions_to_remove {
                    match partition {
                        Some(v) => sectors.push(v),
                        None => {
                            eprintln!("failed to find partition");
                            break;
                        }
                    }
                }

                let partitions = partitions.get_mut(entity).expect("entity does not have partitions");

                info!("Partitioner: removing partititions from {}", &device.display());
                if let Err(why) = remove_partitions(&entities, ped_device, &mut (*partitions).0, &sectors) {
                    eprintln!("failed to remove partitions: {}", why);
                    break
                }
            }

            // TODO: Perform move and resize operations, here.

            // Followed by adding partitions to the disk.
            info!("Partitioner: adding partitions to {}", &device.display());
            let partitions = partitions.get_mut(entity).expect("entity does not have partitions");
            if let Err(why) = add_partitions(
                &entities,
                &mut devices,
                &mut partition_comps,
                &mut format_with,
                &mut luks,
                &mut lvms,
                &device,
                ped_device,
                &mut (*partitions).0,
                &modification.add
            ) {
                eprintln!("failed to add partitions: {}", why);
                break
            }
        }

        info!("Partitioner: job complete in {:?}", Instant::now() - start);
    }
}

fn remove_partitions(
    entities: &Entities,
    device: &mut PedDevice,
    partitions: &mut Vec<Entity>,
    remove: &[(Entity, &PartitionComponent)],
) -> io::Result<()> {
    let disk = &mut open_disk(device)?;
    for (entity, comp) in remove {
        // TODO: handle partitions that are logical.
        remove_partition_by_sector(disk, comp.start_sector)?;
        if let Some(pos) = partitions.iter().position(|e| e == entity) {
            partitions.remove(pos);
        }
        entities.delete(*entity);
    }

    Ok(())
}

#[allow(too_many_arguments)]
fn add_partitions(
    entities: &Entities,
    dev_comps: &mut WriteStorage<DeviceComponent>,
    part_comps: &mut WriteStorage<PartitionComponent>,
    format: &mut WriteStorage<FormatWith>,
    luks: &mut WriteStorage<LuksComponent>,
    lvms: &mut WriteStorage<LvmComponent>,
    path: &Path,
    device: &mut PedDevice,
    partitions: &mut Vec<Entity>,
    add: &[AddPartition]
) -> io::Result<()> {
    let mut partition = PartitionCreate::default();
    partition.path = path.to_path_buf();

    let calc = DiskSectorCalc { sectors: device.length(), sector_size: device.sector_size() };

    for part in add {
        partition.start_sector = calc.get_sector(part.start);
        partition.end_sector = calc.get_sector(part.end);
        info!(
            "Partitioner: creating partition at {}: {} - {}",
            partition.path.display(), partition.start_sector, partition.end_sector
        );

        create_partition(device, &partition)?;

        let (_num, path) = get_partition_id_and_path(path, partition.start_sector as i64)?;
        partitions.push(
            partition_creation(
                entities,
                dev_comps,
                part_comps,
                format,
                luks,
                lvms,
                path,
                partition.start_sector,
                partition.end_sector,
                part.kind.as_ref()
            )
            // TODO: handle this error
            .unwrap()
        )
    }

    Ok(())
}

// TODO: Handle errors
#[allow(too_many_arguments)]
fn partition_creation(
    entities: &Entities,
    dev_comps: &mut WriteStorage<DeviceComponent>,
    part_comps: &mut WriteStorage<PartitionComponent>,
    format: &mut WriteStorage<FormatWith>,
    luks: &mut WriteStorage<LuksComponent>,
    lvms: &mut WriteStorage<LvmComponent>,
    device: PathBuf,
    sector_start: u64,
    sector_end: u64,
    kind: Option<&PartitionVariant>,
) -> Result<Entity, &'static str> {
    assert!(sector_start < sector_end);

    info!("Partitioner: defining partition with {} sectors at {}", sector_end - sector_start, device.display());
    let mut component = PartitionComponent::default();
    component.start_sector = sector_start;
    component.end_sector = sector_end;

    let mut device_comp = DeviceComponent::default();
    device_comp.sectors = sector_end - sector_start;

    let mut entity = entities.build_entity();

    let calc = DiskSectorCalc { sectors: sector_end - sector_start, sector_size: 512 };

    match kind {
        Some(PartitionVariant::Luks { physical_volume, password, file_system }) => {
            info!("Partitioner: creating LUKS volume ({}) at {}", physical_volume, device.display());

            let pass = password.as_ref().expect("LUKS must have a password for now");
            cryptsetup_encrypt(&device, &pass).unwrap();
            cryptsetup_open(&device, &physical_volume, &pass).unwrap();

            let device = PathBuf::from(["/dev/mapper/", &physical_volume].concat());
            pvcreate(&device).unwrap();

            let luks_comp = LuksComponent {
                physical_volume: physical_volume.clone(),
                password: password.clone(),
                child: match file_system {
                    Some(file_system) => {
                        Some(partition_creation(
                            entities,
                            dev_comps,
                            part_comps,
                            format,
                            luks,
                            lvms,
                            device,
                            sector_start,
                            sector_end,
                            Some(file_system.as_ref())
                        )?)
                    }
                    None => None
                }
            };

            entity = entity.with(luks_comp, luks);
        }
        Some(PartitionVariant::Lvm { volume_group, table }) => {
            // TODO: Handle multiple volume groups somehow?
            vgcreate(volume_group, [&device].iter()).unwrap();

            let mut lvm_component = LvmComponent {
                volume_group: volume_group.clone(),
                partitions: Vec::new()
            };

            let nparts = table.len();
            for (id, partition) in table.iter().enumerate() {
                let lv_name = partition.0.as_ref();

                let start = calc.get_sector(partition.1.start);
                let end = calc.get_sector(partition.1.end);

                let size_in_bytes = if id == nparts - 1 {
                    None
                } else {
                    Some((end - start) * 512)
                };

                info!("Partitioner: creating logical volume ({}-{} {} sectors)", volume_group, lv_name, end - start);
                lvcreate(&volume_group, &lv_name, size_in_bytes).unwrap();

                // TODO: Handle names with dashes in them.
                let device = PathBuf::from([
                    "/dev/mapper/",
                    &volume_group,
                    "-",
                    &lv_name
                ].concat());

                let pentity = partition_creation(
                    entities,
                    dev_comps,
                    part_comps,
                    format,
                    luks,
                    lvms,
                    device,
                    start,
                    end,
                    partition.1.kind.as_ref()
                )?;

                lvm_component.partitions.push(pentity);
            }

            entity = entity.with(lvm_component, lvms);
        }
        Some(PartitionVariant::FileSystem { label, file_system }) => {
            info!("Partitioner: assigning {} to be formatted with {}", device.display(), file_system);
            entity = entity.with(FormatWith(*file_system), format);
        }
        None => ()
    }

    device_comp.device = device;

    let pentity = entity
        .with(device_comp, dev_comps)
        .with(component, part_comps)
        .build();

    Ok(pentity)
}

struct DiskSectorCalc {
    sectors: u64,
    sector_size: u64
}

impl SectorExt for DiskSectorCalc {
    fn get_sectors(&self) -> u64 { self.sectors }
    fn get_sector_size(&self) -> u64 { self.sector_size }
}
