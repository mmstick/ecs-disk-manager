mod builder;
mod components;
mod entities;
mod systems;
mod traits;

pub use self::builder::*;
pub use self::components::*;
pub use self::entities::*;
pub use self::traits::*;

use dbus_udisks2::UDisks2;
use disk_types::FileSystem;
use self::systems::*;
use specs::prelude::*;
use specs::world::{EntitiesRes, Index};
use std::ops::{Deref, DerefMut};
use std::path::Path;
use std::{io, mem};

#[derive(Default)]
pub struct DiskErrors(pub Vec<io::Error>);

impl Deref for DiskErrors {
    type Target = Vec<io::Error>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl DerefMut for DiskErrors {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

/// The world manages state for all devices and their data, as well as the systems which are
/// applied to them. Everything begins and ends with the world.
pub struct DeviceWorld {
    world: World,
    udisks: UDisks2,
}

impl DeviceWorld {
    pub fn new() -> io::Result<Self> {
        // Make a private system dbus connection.
        let udisks = UDisks2::new().map_err(|why| io::Error::new(
            io::ErrorKind::Other,
            format!("udisks2 dbus error: {}", why)
        ))?;

        // A storage must be initialized for every component type that an entity in the world may hold.
        let mut world = World::new();
        world.register::<DeviceComponent>();
        world.register::<DiskComponent>();
        world.register::<FileSystemComponent>();
        world.register::<FormatTable>();
        world.register::<FormatWith>();
        world.register::<PartitionComponent>();
        world.register::<PartitionModifications>();
        world.register::<Partitions>();
        world.register::<Swapspace>();
        world.register::<LuksComponent>();
        world.register::<LvmComponent>();

        // Resources are like global state, which all of the systems in this world can share.
        world.add_resource::<DiskErrors>(Default::default());

        // Initialize the world before returning it.
        let mut world = Self { world, udisks };
        world.update()?;

        Ok(world)
    }

    /// Update the world via comparing system state.
    pub fn update(&mut self) -> io::Result<()> {
        // TODO: How to respond to system changes in encrypted volumes?
        let world = &mut self.world;
        world.delete_all();

        let disks = ::dbus_udisks2::Disks::new(&self.udisks);
        for drive in disks.devices {
            let mut partitions = Vec::new();
            if ! drive.partitions.is_empty() {
                for partition in &drive.partitions {
                    if let Some(ref pblock) = partition.partition {
                        let mut pentity = world.create_entity()
                            .with(DeviceComponent::new(partition))
                            .with(PartitionComponent::new(pblock));

                        if let Some(active) = partition.swapspace {
                            pentity = pentity.with(Swapspace { active });
                        } else if let Some(fs) = FileSystemComponent::from_block(partition) {
                            pentity = pentity.with(fs);
                            // TODO: Handle LUKS and LVM Components
                        }

                        partitions.push(pentity.build());
                    }
                }
            }

            let parent = &drive.parent;
            world.create_entity()
                .with(DeviceComponent::new(parent))
                .with(DiskComponent::new(parent, &drive.drive))
                .with(Partitions(partitions))
                .build();
        }

        world.maintain();

        Ok(())
    }

    /// Apply all queued actions to the world, and return the errors that may have occurred.
    pub fn dispatch(&mut self) -> Option<Vec<io::Error>> {
        info!("dispatching all disk operations");

        // Apply `wipefs` and `mklabel` to all disks that requested it.
        TableMaker.run_now(&self.world.res);
        self.world.maintain();

        if let Some(errors) = self.get_errors() {
            return Some(errors);
        }

        // Move, resize, add, and delete partitions from the devices in the system.
        Partitioner.run_now(&self.world.res);
        self.world.maintain();

        if let Some(errors) = self.get_errors() {
            return Some(errors);
        }

        // Format each partition that requires it.
        Formatter.run_now(&self.world.res);
        self.get_errors()
    }

    /// Locates a device, which may be a disk or partition.
    pub fn find_device<'b, T>(
        &'b mut self,
        path: impl AsRef<Path>,
        mut func: impl FnMut(DeviceEntity<'b>) -> T,
    ) -> Option<T> {
        let path = path.as_ref();
        let entity = {
            let entities = self.world.read_resource::<EntitiesRes>();
            let devices = self.world.read_storage::<DeviceComponent>();

            // Find the partition with the specified path.
            (&*entities, &devices)
                .join()
                .find(|(_, device)| device.device == path)
                .map(|(e, _)| e)
        };

        // Allow the caller to handle the this entity.
        entity.map(move |entity| {
            func(DeviceEntity {
                world: &mut self.world,
                entity,
            })
        })
    }

    /// Helper function to find the disk entity with the given path.
    fn find_disk_inner(&self, path: impl AsRef<Path>) -> Option<Entity> {
        let path = path.as_ref();
        let entities = self.world.read_resource::<EntitiesRes>();
        let devices = self.world.read_storage::<DeviceComponent>();
        let disks = self.world.read_storage::<DiskComponent>();

        // Find the partition with the specified path.
        (&*entities, &devices, &disks)
            .join()
            .find(|(_, device, _)| device.device == path)
            .map(|(e, _, _)| e)
    }

    /// Finds the disk entity that has the given path, and passes it into the given closure.
    ///
    /// Returns `None` if the entity was not found, else returns the value returned by the inner closure.
    pub fn find_disk<T>(
        &self,
        path: impl AsRef<Path>,
        mut func: impl for<'b> FnMut(DiskEntity<'b>) -> T,
    ) -> Option<T> {
        let entity = self.find_disk_inner(path);
        let world = &self.world;

        entity.map(move |entity| func(DiskEntity { world, entity }))
    }

    /// Returns an iterator to iterate through each disk entity in the world.
    pub fn get_disks(&self) -> DiskIter {
        let world = &self.world;
        DiskIter {
            world,
            id: 0,
            entities: {
                let disks = world.read_storage::<DiskComponent>();
                (&*world.entities(), &disks).join()
                    .map(|(entity, _)| entity.id())
                    .collect::<Vec<Index>>()
            }
        }
    }

    // Resources

    /// Allow the caller to check for any errors that may have happened when formatting.
    fn get_errors(&mut self) -> Option<Vec<io::Error>> {
        let mut errors = self.world.write_resource::<DiskErrors>();
        if errors.0.is_empty() {
            None
        } else {
            let mut out = Vec::new();
            mem::swap(&mut errors.0, &mut out);
            Some(out)
        }
    }
}

pub struct DiskIter<'a> {
    world: &'a World,
    entities: Vec<Index>,
    id: usize,
}

impl<'a> Iterator for DiskIter<'a> {
    type Item = DiskEntity<'a>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.id < self.entities.len() {
            let entity = self.world.entities().entity(self.entities[self.id]);
            self.id += 1;
            Some(DiskEntity { world: self.world, entity})
        } else {
            None
        }
    }
}
