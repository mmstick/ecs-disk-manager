use dbus_udisks2::Block;
use specs::prelude::*;
use std::path::PathBuf;

#[derive(Component, Default)]
#[storage(VecStorage)]
pub struct DeviceComponent {
    /// The path where this device is located.
    pub device: PathBuf,
    /// ???
    pub id: String,
    /// Where this device is currently mounted at.
    pub mount_points: Vec<PathBuf>,
    /// The number of sectors in this device.
    pub sectors: u64
}

impl DeviceComponent {
    pub fn new(block: &Block) -> DeviceComponent {
        Self {
            device: block.preferred_device.clone(),
            sectors: block.size / 512,
            mount_points: block.mount_points.clone(),
            id: block.id.clone()
        }
    }
}
