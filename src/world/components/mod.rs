mod device;
mod disk;
mod logical;
mod partition;

pub use self::device::*;
pub use self::disk::*;
pub use self::logical::*;
pub use self::partition::*;

use dbus_udisks2::Block;
use disk_types::FileSystem;
use specs::prelude::*;

#[derive(Component, Copy, Clone)]
#[storage(VecStorage)]
pub struct FileSystemComponent(pub FileSystem);

impl FileSystemComponent {
    pub(crate) fn from_block(block: &Block) -> Option<Self> {
        let fs = block.id_type.as_ref().and_then(|t| match t.as_str() {
            "vfat" => block.id_version.as_ref().and_then(|v| v.parse::<FileSystem>().ok()),
            v => v.parse::<FileSystem>().ok()
        });

        fs.map(FileSystemComponent)
    }
}

#[derive(Component)]
#[storage(VecStorage)]
pub struct Partitions(pub Vec<Entity>);

#[derive(Component)]
#[storage(VecStorage)]
pub struct Swapspace {
    pub active: bool
}
