use specs::prelude::*;

#[derive(Clone, Component, Debug)]
#[storage(VecStorage)]
pub struct LvmComponent {
    pub volume_group: String,
    pub partitions: Vec<Entity>,
}

#[derive(Clone, Component, Debug)]
#[storage(VecStorage)]
pub struct LuksComponent {
    pub physical_volume: String,
    pub password: Option<String>,
    pub child: Option<Entity>,
}
