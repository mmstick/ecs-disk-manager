use dbus_udisks2::Partition as BlockPartition;
use disk_types::FileSystem;
use world::builder::*;
use specs::prelude::*;

#[derive(Clone, Component, Default)]
#[storage(VecStorage)]
pub struct PartitionModifications {
    pub add: Vec<AddPartition>,
    pub remove: Vec<Entity>,
}

#[derive(Component, Default)]
#[storage(VecStorage)]
pub struct PartitionComponent {
    pub start_sector: u64,
    pub end_sector: u64,
    pub number: u32,
    pub uuid: String,
    pub name: String,
    pub table: String,
    pub type_: String,
}

impl PartitionComponent {
    pub fn new(partition: &BlockPartition) -> PartitionComponent {
        let start_sector = partition.offset / 512;
        Self {
            start_sector,
            end_sector: partition.size / 512 + start_sector,
            uuid: partition.uuid.clone(),
            number: partition.number,
            type_: partition.type_.clone(),
            table: partition.table.clone(),
            name: partition.name.clone()
        }
    }
}

/// Designates this device to be formatted with the given file system.
#[derive(Clone, Copy, Component)]
#[storage(VecStorage)]
pub struct FormatWith(pub FileSystem);

/// Designates the name to apply to a file system.
#[derive(Component)]
#[storage(VecStorage)]
pub struct PartitionName(pub String);
