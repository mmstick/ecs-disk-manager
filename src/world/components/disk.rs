use dbus_udisks2::{Block, Drive};
use disk_types::PartitionTable;
use specs::prelude::*;


#[derive(Component)]
#[storage(VecStorage)]
pub struct FormatTable(pub PartitionTable);

#[derive(Component)]
#[storage(VecStorage)]
pub struct DiskComponent {
    pub partition_table: Option<PartitionTable>,
    pub model: String,
    pub serial: String,
    pub removable: bool,
}

impl DiskComponent {
    pub fn new(block: &Block, drive: &Drive) -> Self {
        let mut disk = Self {
            model: drive.model.clone(),
            serial: drive.serial.clone(),
            partition_table: None,
            removable: drive.removable,
        };

        if let Some(ref table) = block.table {
            disk.partition_table = match table.type_.as_str() {
                "gpt" => Some(PartitionTable::Gpt),
                "mbr" => Some(PartitionTable::Msdos),
                _ => None,
            };
        }

        disk
    }
}
