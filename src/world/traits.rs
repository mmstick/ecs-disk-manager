//! Some convenience methods for interacting with the world.

use disk_types::FileSystem;
use super::*;
use specs::prelude::*;

/// Generic helper trait to make working with a world simpler.
pub(crate) trait WorldExt {
    fn component_update<T: Component>(&self, device: Entity, with: T);
    fn component_remove<T: Component>(&self, device: Entity);
}

impl WorldExt for World {
    /// Insert or replace the component currently stored for this `device`.
    fn component_update<T: Component>(&self, device: Entity, with: T) {
        let mut storage = self.write_storage::<T>();
        storage.remove(device);
        let _ = storage.insert(device, with);
    }

    /// Removes the specified component from this `device`.
    fn component_remove<T: Component>(&self, device: Entity) {
        self.write_storage::<T>().remove(device);
    }

    // TODO: Why does the type for F need to be specified by the caller?
    // fn component_append<T, F>(&mut self, device: Entity, mut append: F)
    // where T: Default + Component,
    //       F: for<'b> FnMut(&'b mut T)
    // {
    //     let mut storage = self.world.write_storage::<T>();
    //     match storage.get_mut(device) {
    //         Some(component) => append(component),
    //         None => {
    //             let mut default: T = Default::default();
    //             append(&mut default);
    //             storage.insert(device, default);
    //         }
    //     }
    // }
}

pub trait DeviceEntityTrait {
    fn get_world(&mut self) -> &mut World;
    fn get_entity(&self) -> Entity;
}

/// A type which implements this may be formatted.
pub trait Formattable: DeviceEntityTrait {
    /// Specifies to format the given device with the specified file system.
    fn format_with(&mut self, fs: FileSystem) {
        let device = self.get_entity();
        let world = self.get_world();
        world.component_update(device, FormatWith(fs));
    }
}
