use super::super::*;
use specs::prelude::*;
use disk_types::{BlockDeviceExt, PartitionTable};

pub struct DiskEntity<'a> {
    pub(crate) world: &'a World,
    pub(crate) entity: Entity,
}

impl<'a> DiskEntity<'a> {
    pub fn get_data(&self) -> DiskComponents {
        DiskComponents {
            entity: self.entity,
            world: self.world,
            devices: self.world.read_storage::<DeviceComponent>(),
            disks: self.world.read_storage::<DiskComponent>(),
            partitions: self.world.read_storage::<Partitions>(),
            filesystem: self.world.read_storage::<FileSystemComponent>(),
        }
    }

    /// Add partitions to a device.
    pub fn partition_add(&self, partition: impl Into<AddPartition>) {
        // TODO: Validate if the partition can be added.
        // TODO: Use component_append instead.
        let partition = partition.into();
        let mut storage = self.world.write_storage::<PartitionModifications>();
        let partition = match storage.get_mut(self.entity) {
            Some(component) => {
                component.add.push(partition);
                None
            }
            None => {
                let mut default = PartitionModifications::default();
                default.add.push(partition);
                Some(default)
            }
        };

        if let Some(default) = partition {
            storage.insert(self.entity, default);
        }
    }

    /// Specifies to remove the `partition` from the `device`.
    pub fn partition_remove(&self, partition: Entity) {
        // TODO: Validate that the partition can be removed.
        // TODO: Use component_append instead.
        let mut storage = self.world.write_storage::<PartitionModifications>();
        let partition = match storage.get_mut(self.entity) {
            Some(component) => {
                component.remove.push(partition);
                None
            }
            None => {
                let mut default = PartitionModifications::default();
                default.remove.push(partition);
                Some(default)
            }
        };

        if let Some(default) = partition {
            storage.insert(self.entity, default);
        }
    }

    /// Specifies that the device should be formatted with the given table.
    pub fn mklabel(&self, table: PartitionTable) {
        self.world.component_update(self.entity, FormatTable(table));
    }
}

pub struct DiskComponents<'a> {
    entity: Entity,
    world: &'a World,
    devices: ReadStorage<'a, DeviceComponent>,
    disks: ReadStorage<'a, DiskComponent>,
    partitions: ReadStorage<'a, Partitions>,
    filesystem: ReadStorage<'a, FileSystemComponent>,
}

impl<'a> DiskComponents<'a> {
    fn get_device_component(&self) -> &DeviceComponent {
        self.devices
            .get(self.entity)
            .expect("disk does not have device component")
    }

    fn get_disk_component(&self) -> &DiskComponent {
        self.disks
            .get(self.entity)
            .expect("disk does not have disk component")
    }

    fn get_partitions_component(&self) -> Option<&Partitions> {
        self.partitions.get(self.entity)
    }

    pub fn get_device_path(&self) -> &Path {
        self.get_device_component().device.as_ref()
    }

    pub fn get_file_system(&self) -> Option<FileSystem> {
        self.filesystem.get(self.entity).map(|x| x.0)
    }

    pub fn get_model_string(&self) -> &str {
        self.get_disk_component().model.as_ref()
    }

    pub fn get_partition_table(&self) -> Option<PartitionTable> {
        self.get_disk_component().partition_table
    }

    pub fn get_partitions(&self) -> Option<impl Iterator<Item = PartitionEntity>> {
        let world = self.world;
        self.get_partitions_component().map(|partitions| {
            partitions.0.iter().map(move |&entity| PartitionEntity { world, entity })
        })
    }

    pub fn get_sectors(&self) -> u64 {
        self.get_device_component().sectors
    }
}
