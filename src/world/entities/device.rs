use super::super::*;
use specs::prelude::*;

pub struct DeviceEntity<'a> {
    pub(crate) world: &'a mut World,
    pub(crate) entity: Entity,
}

impl<'a> DeviceEntityTrait for DeviceEntity<'a> {
    fn get_world(&mut self) -> &mut World {
        &mut self.world
    }

    fn get_entity(&self) -> Entity {
        self.entity
    }
}

impl<'a> Formattable for DeviceEntity<'a> {}
