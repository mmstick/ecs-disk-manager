use disk_types::FileSystem;
use super::super::*;
use libparted::PartitionFlag;
use specs::prelude::*;
use std::path::Path;
use disk_types::{BlockDeviceExt, PartitionExt, PartitionType};

pub struct PartitionEntity<'a> {
    pub(crate) world: &'a World,
    pub(crate) entity: Entity,
}

impl<'a> PartitionEntity<'a> {
    pub fn get_data(&self) -> PartitionComponents {
        PartitionComponents {
            entity: self.entity,
            device: self.world.read_storage::<DeviceComponent>(),
            partition: self.world.read_storage::<PartitionComponent>(),
            filesystem: self.world.read_storage::<FileSystemComponent>(),
        }
    }
}

pub struct PartitionComponents<'a> {
    entity: Entity,
    device: ReadStorage<'a, DeviceComponent>,
    partition: ReadStorage<'a, PartitionComponent>,
    filesystem: ReadStorage<'a, FileSystemComponent>,
}

impl<'a> BlockDeviceExt for PartitionComponents<'a> {
    fn get_device_path(&self) -> &Path {
        self.get_device_component().device.as_ref()
    }

    fn get_mount_point(&self) -> Option<&Path> {
        None
    }
}

impl<'a> ReadPartition for PartitionComponents<'a> {
    fn get_component(&self) -> &PartitionComponent {
        self.partition
            .get(self.entity)
            .expect("partition does not have partition component")
    }

    fn get_device_component(&self) -> &DeviceComponent {
        self.device
            .get(self.entity)
            .expect("partition does not have a device component")
    }
}

impl<'a> PartitionExt for PartitionComponents<'a> {
    fn get_file_system(&self) -> Option<FileSystem> {
        self.filesystem.get(self.entity).map(|x| x.0)
    }

    fn get_partition_flags(&self) -> &[PartitionFlag] {
        &[]
    }

    fn get_partition_label(&self) -> Option<&str> {
        None
    }

    fn get_partition_type(&self) -> PartitionType {
        PartitionType::Primary
    }

    fn get_sector_end(&self) -> u64 {
        self.get_component().end_sector
    }

    fn get_sector_start(&self) -> u64 {
        self.get_component().start_sector
    }
}


pub trait ReadPartition {
    fn get_component(&self) -> &PartitionComponent;

    fn get_device_component(&self) -> &DeviceComponent;

    // // Partition Info
    //
    fn get_device_number(&self) -> u32 {
        self.get_component().number
    }

    fn get_type(&self) -> &str {
        self.get_component().type_.as_str()
    }
}
