use bootloader::Bootloader;
use libparted::{Device, Disk as PedDisk, DiskType as PedDiskType};
use std::ffi::OsString;
use std::fs::File;
use std::io::{self, Write};
use std::path::Path;
use std::process::{Command, Stdio};

/// Gets and opens a `libparted::Device` from the given name.
pub(crate) fn open_device<'a, P: AsRef<Path>>(name: P) -> io::Result<Device<'a>> {
    let device = name.as_ref();
    Device::new(device).map_err(|why| {
        io::Error::new(
            io::ErrorKind::Other,
            format!("failed to open device at {:?}: {}", device, why),
        )
    })
}

/// Opens a `libparted::Disk` from a `libparted::Device`.
pub(crate) fn open_disk<'a>(device: &'a mut Device) -> io::Result<PedDisk<'a>> {
    eprintln!("opening disk at {}", device.path().display());
    let device = device as *mut Device;
    unsafe {
        match PedDisk::new(&mut *device) {
            Ok(disk) => Ok(disk),
            Err(_) => {
                eprintln!("unable to open disk; creating new table on it");
                PedDisk::new_fresh(
                    &mut *device,
                    match Bootloader::detect() {
                        Bootloader::Bios => PedDiskType::get("msdos").unwrap(),
                        Bootloader::Efi => PedDiskType::get("gpt").unwrap(),
                    },
                )
            }
        }
    }
}

/// Attempts to commit changes to the disk, return a `DiskError` on failure.
pub(crate) fn commit(disk: &mut PedDisk) -> io::Result<()> {
    eprintln!("committing changes to {}", unsafe {
        disk.get_device().path().display()
    });

    disk.commit()
}

/// Flushes the OS cache, return a `DiskError` on failure.
pub(crate) fn sync(device: &mut Device) -> io::Result<()> {
    eprintln!("syncing device at {}", device.path().display());
    device.sync()
}

pub(crate) fn open<P: AsRef<Path>>(path: P) -> io::Result<File> {
    File::open(&path).map_err(|why| {
        io::Error::new(
            io::ErrorKind::Other,
            format!("unable to open file at {:?}: {}", path.as_ref(), why),
        )
    })
}

pub(crate) fn exec(
    cmd: &str,
    stdin: Option<&[u8]>,
    valid_codes: Option<&'static [i32]>,
    args: &[OsString],
) -> io::Result<()> {
    eprintln!("executing {} with {:?}", cmd, args);

    let mut child = Command::new(cmd)
        .args(args)
        .stdin(if stdin.is_some() {
            Stdio::piped()
        } else {
            Stdio::null()
        }).stdout(Stdio::null())
        .spawn()?;

    if let Some(stdin) = stdin {
        child
            .stdin
            .as_mut()
            .expect("stdin not obtained")
            .write_all(stdin)?;
    }

    let status = child.wait()?;
    let success = status.success() || valid_codes.map_or(false, |codes| {
        status.code().map_or(false, |code| codes.contains(&code))
    });

    if success {
        Ok(())
    } else {
        Err(io::Error::new(
            io::ErrorKind::Other,
            format!(
                "{} failed with status: {}",
                cmd,
                match status.code() {
                    Some(code) => format!("{} ({})", code, io::Error::from_raw_os_error(code)),
                    None => "unknown".into(),
                }
            ),
        ))
    }
}
